﻿using System.Collections.Generic;
using UnityEngine;
using UniversalRenderer.Configs;
using UniversalRenderer.Framework.Utils;
using UniversalRenderer.RendererSystem;

namespace UniversalRenderer.Controllers
{
	public class RenderController : SingletonMonoBehaviour<RenderController>
	{
		[SerializeField] private RenderConfig _config;

		private static readonly List<RenderContainerItem> _items = new();

		public static RenderTexture Render(GameObject gameObject, Vector2 size, Vector2 boundsOffset)
		{
			var item = Instantiate(Main._config.RenderPrefab,  Main.transform);
			item.InitObjectRenderer(Main._config, gameObject, size, boundsOffset);
			_items.Add(item);

			Main.CalculatePosition();
			return item.GetRenderTexture();
		}

		public static void DisposeRender(RenderTexture renderTexture)
		{
			var item = _items.Find(x => x.GetRenderTexture() == renderTexture);
			if (item == null)
			{
				return;
			}

			_items.Remove(item);
			Destroy(item.gameObject);

			Main.CalculatePosition();
		}

		private void CalculatePosition()
		{
			for (var i = 0; i < _items.Count; i++)
			{
				var item = _items[i];
				var position = item.transform.position;
				position.x = i * 5;
				item.transform.position = position;
			}
		}

		protected override void OnDestroyInner()
		{
			ClearRenderer();
		}

		private void ClearRenderer()
		{
			_items.ForEach(x => Destroy(x.gameObject));
			_items.Clear();
		}
	}
}