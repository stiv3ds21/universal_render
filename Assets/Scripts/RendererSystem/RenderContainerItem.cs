using UnityEngine;
using UniversalRenderer.Configs;

namespace UniversalRenderer.RendererSystem
{
    public class RenderContainerItem : MonoBehaviour
    {
        [SerializeField] private Camera _camera;
        [SerializeField] private Transform _targetRendererTarget;

        private RenderConfig _config;
        private GameObject _targetObject;


        public void InitObjectRenderer(RenderConfig config, GameObject targetObject, Vector2 size, Vector2 boundsOffset)
        {
            _config = config;
            _targetObject = targetObject;

            targetObject.transform.SetParent(_targetRendererTarget, false);
            targetObject.transform.localPosition = Vector3.zero;
            targetObject.layer = LayerMask.NameToLayer("RendererLayer");

            var boundsTarget = targetObject.GetComponent<MeshFilter>().sharedMesh.bounds;

            var radAngle = _camera.fieldOfView * Mathf.Deg2Rad;
            var radHFOV = 2 * Mathf.Atan(Mathf.Tan(radAngle / 2) * _camera.aspect);

            var widthZoom = (boundsTarget.size.x + boundsOffset.x) / 2.0f / Mathf.Tan(radHFOV / 2.0f);
            var heightZoom = (boundsTarget.size.y + boundsOffset.y) / 2.0f / Mathf.Tan(radAngle / 2.0f);

            var cameraPosition = _camera.transform.position;
            cameraPosition.z = -Mathf.Max(widthZoom, heightZoom);
            _camera.transform.position = cameraPosition;
            _camera.targetTexture = CreateRenderTexture(new Vector2Int((int)size.x, (int)size.y));
        }

        private void OnDestroy()
        {
            Dispose();
        }

        private void Dispose()
        {
            RenderTexture.ReleaseTemporary(_camera.targetTexture);
        }

        public RenderTexture GetRenderTexture()
        {
            return _camera.targetTexture;
        }

        private RenderTexture CreateRenderTexture(Vector2Int size)
        {
            var renderTexture = RenderTexture.GetTemporary(size.x, size.y);
            return renderTexture;
        }
    }
}
