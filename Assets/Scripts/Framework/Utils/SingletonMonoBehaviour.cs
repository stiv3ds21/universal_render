﻿using UnityEngine;

namespace UniversalRenderer.Framework.Utils
{
    public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool _shuttingDown;
        private static readonly object _lock = new();
        private static T _main;

        public static T Main
        {
            get
            {
                if (_shuttingDown) _main = null;

                lock (_lock)
                {
                    if (_main == null)
                    {
                        _main = (T)FindObjectOfType(typeof(T));
                        if (_main == null) _main = Create();
                    }

                    return _main;
                }
            }
        }

        private void OnApplicationQuit()
        {
            _shuttingDown = true;
        }

        private void OnDestroy()
        {
            _shuttingDown = true;
            OnDestroyInner();
        }

        protected abstract void OnDestroyInner();

        public static T Create()
        {
            if (!Application.isPlaying) return default;
            var singletonObject = new GameObject();
            var instance = singletonObject.AddComponent<T>();
            singletonObject.name = typeof(T).Name;
            return instance;
        }
    }
}