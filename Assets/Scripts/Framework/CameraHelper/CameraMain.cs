﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace UniversalRenderer.Framework.CameraHelper
{
    public static class CameraMain
    {
        private static readonly List<CameraComponent> _cameraComponents = new List<CameraComponent>();

        public static IReadOnlyList<CameraComponent> CameraComponents => _cameraComponents;

        public static Camera MainCamera => GetCamera(CameraKey.Main);

        public static Camera GetCamera(CameraKey key)
        {
            var item = _cameraComponents.Find(x => x.IsCameraType(key));
            return item == null ? null : item.MyCamera;
        }

        public static void AddCamera(CameraComponent cameraComponent)
        {
            if (cameraComponent.IsCameraType(CameraKey.Main) == false)
            {
                AdditionalCameraData(MainCamera, cameraComponent.MyCamera);
            }

            _cameraComponents.Add(cameraComponent);
        }

        private static void AdditionalCameraData(Camera mainCamera, Camera camera)
        {
            var cameraData = mainCamera.GetUniversalAdditionalCameraData();
            cameraData.cameraStack.Add(camera);
            var list = cameraData.cameraStack.OrderBy(x => x.depth).ToList();
            cameraData.cameraStack.Clear();
            list.ForEach(x => cameraData.cameraStack.Add(x));
        }

        public static void RemoveCamera(CameraComponent cameraComponent)
        {
            var cameraData = MainCamera.GetUniversalAdditionalCameraData();
            cameraData.cameraStack.Remove(cameraComponent.MyCamera);
            _cameraComponents.Remove(cameraComponent);
        }
    }

    public enum CameraKey
    {
        Main = 0,
        Renderer = 100
    }
}