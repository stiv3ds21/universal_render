using UnityEngine;

namespace UniversalRenderer.Framework.CameraHelper
{
    [RequireComponent(typeof(Camera))]
    public class CameraComponent : MonoBehaviour
    {
        [SerializeField] private CameraKey _cameraType;
        
        private Camera _camera;
        public Camera MyCamera => _camera != null ? _camera : _camera = GetComponent<Camera>();

        private void Awake()
        {
            CameraMain.AddCamera(this);
        }
        
        private void OnDestroy()
        {
            CameraMain.RemoveCamera(this);
        }

        public bool IsCameraType(CameraKey value) => _cameraType == value;
    }
}