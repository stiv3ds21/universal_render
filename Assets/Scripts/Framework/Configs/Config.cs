using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UniversalRenderer.Framework.Configs
{
    public interface IConfig: IIdentification
    {
        string ID { get; }
    }
    
    public abstract class Config : ScriptableObject, IConfig
    {
        [SerializeField] private string _id;
        public string ID => _id;
    }
}
