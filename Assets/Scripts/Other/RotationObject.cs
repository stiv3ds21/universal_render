using UnityEngine;

namespace UniversalRenderer
{
    public class RotationObject : MonoBehaviour
    {
        private Transform _transform;

        public Transform Transform => _transform != null ? _transform : _transform = GetComponent<Transform>();

        private void Update()
        {
            Transform.Rotate(0, 12.0f * Time.deltaTime, 0);
        }
    }
}