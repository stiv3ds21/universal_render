﻿using UnityEngine;
using UniversalRenderer.Framework.Configs;
using UniversalRenderer.RendererSystem;

namespace UniversalRenderer.Configs
{
    [CreateAssetMenu(menuName = "UniversalRenderer/Configs/RenderConfig", fileName = "RenderConfig")]
    public class RenderConfig : Config
    {
        [SerializeField] private RenderContainerItem _renderPrefab;
        
        public RenderContainerItem RenderPrefab => _renderPrefab;
    }
}