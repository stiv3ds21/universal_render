using UnityEngine;
using UnityEngine.UI;
using UniversalRenderer.Controllers;

namespace UniversalRenderer
{
    public class RenderItemUI : MonoBehaviour
    {
        [SerializeField] private RawImage _rawImage;
        [SerializeField] private GameObject _renderTarget;

        private RenderTexture _renderTexture;

        private void Start()
        {
            var rect = _rawImage.GetComponent<RectTransform>().rect;
            var size = new Vector2(rect.width, rect.height);
            var boundsOffset = new Vector2(1, 1);
            var obj = Instantiate(_renderTarget);
            _renderTexture = RenderController.Render(obj, size, boundsOffset);
            _rawImage.texture = _renderTexture;
        }

        private void OnDestroy()
        {
            RenderController.DisposeRender(_renderTexture);
        }
    }
}